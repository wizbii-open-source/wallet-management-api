Description
-----------

L'objectif de ce test technique est de reproduire un process que nous suivons dans la quasi totalité de nos développements : 
-  récupérer les sources d'une application existante (dans le cas précis, il faudra faire un fork du repository)
-  le comprendre
-  arriver à le faire fonctionner
-  le faire évoluer pour prendre en compte les besoins exprimés par le Product Owner
-  envoyer une Merge Request à l'équipe expliquant ce qui a été réalisé

Cette application de test est très simple : elle permet de gérer des portemonnaies électroniques een faisant des virements d'un portemonnaie (Wallet) à un autre. Pour des questions de simplicité du test, plusieurs choix ont été réalisés dans ce code :
-  le code utilise lee bundle [Mongo Bundle](https://gitlab.com/wizbii-open-source/mongo-bundle) permettant d'exécuter un mongodb en mémoire (pas  besoin d'avoir une instance de mongodb fonctionnelle pour ce test)
-  l'authentification n'est pas prise en charge : n'importe qui peut donc faire des virements pour le moment :)

L'objectif de ce test est de nous montrer comment vous travaillez à l'heure actuelle ou comment vous souhaitez travailler. Il existe en effet plusieurs possibilités pour réaliser les besoins exprimés ci-dessous. Choisissez celle qui vous correspond le mieux, expliquez la dans votre MR et venez la défendre devant d'autres développeurs sur un entretien technique :)

En règle générale, 2 heures sont nécessaires à la réalisation de ce test.

Besoins du PO
-------------

Le Product Owner a exprimé plusieurs besoins : 

###### Validation des transactions

Lors de la création d'une transaction, il faut vérifier que le wallet qui envoie l'argent dispose bien des fonds suffisants pour effectuer cette opération. De la même manière, il faut s'assurer que le wallet recevant les fonds ne dépasse pas son maximum autorisé.

Tips
-  un squelette de test unitaire est disponible dans la classe `TransactionOperationTest`. Vous pouvez l'exécuter avec la commande : `./vendor/bin/phpunit --testdox --colors=always --group transaction-operation`  
-  le Controller `TransactionController` gère déjà la création des transactions mais n'effectue aucune opération de validation pour le moment

###### Management des wallets

Pour le moment,  il n'est pas possible, via HTTP, de créer des wallets. Pouvez-vous ajouter le support pour les opérations suivantes :
-  création d'un wallet. Pour le moment, il n'est pas nécessaire de vérifier que le groupe auquel est rattaché le wallet existe. Lors de la création d'un wallet, la balance est forcément être positive.
-  lister tous les wallets pour un groupe en retournant les plus récents en premier. Une pagination devra être fourni dans ce WebService
-  vérouiller un wallet. Dans ce cas, aucune transaction ne peut provenir ou arriver sur ce wallet

###### Documentation

Les développeurs Frontend remontent régulièrement qu'ils ont du mal à utiliser cette API car elle manque de documentation. Pouvez-vous ajouter une documentation à destination des développeurs Frontend pour les aider dans leurs tâches actuelles.
Dans le cadre de ce test, il n'est pas nécessaire d'être exhaustif mais plutôt de poser les bases de cette documentation.

