<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Wizbii\JsonSerializerBundle\JsonSerializerBundle::class => ['all' => true],
    Wizbii\MongoBundle\WizbiiMongoBundle::class => ['all' => true],
];
