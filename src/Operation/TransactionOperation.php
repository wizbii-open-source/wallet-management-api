<?php

namespace App\Operation;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\SignedTransaction;
use App\Repository\WalletRepository;

class TransactionOperation
{
    private WalletRepository $walletRepository;

    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

    public function createTransaction(RequestedTransaction $requestedTransaction): SignedTransaction
    {
        throw new \RuntimeException();
    }
}
