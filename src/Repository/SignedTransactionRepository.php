<?php

namespace App\Repository;

use App\Model\Transaction\SignedTransaction;

interface SignedTransactionRepository
{
    public function create(SignedTransaction $transaction);

    public function get(string $transactionId): SignedTransaction;

    /** @return SignedTransaction[] */
    public function findTransactionsSortedByDateDescending(string $walletId, int $rows, int $offset): array;
}
