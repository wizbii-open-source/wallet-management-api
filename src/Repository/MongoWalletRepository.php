<?php

namespace App\Repository;

use App\Exception\WalletIdAlreadyTaken;
use App\Model\Wallet\Wallet;
use Wizbii\MongoBundle\MongoClientBuilderInterface;
use Wizbii\MongoBundle\MongoClientInterface;

class MongoWalletRepository implements WalletRepository
{
    private MongoClientInterface $mongoClient;

    public function __construct(MongoClientBuilderInterface $mongoClientBuilder)
    {
        $this->mongoClient = $mongoClientBuilder->buildFor('wallet', 'wallet', Wallet::class);
    }

    public function create(Wallet $wallet): void
    {
        if ($this->mongoClient->has($wallet->getId())) {
            throw new WalletIdAlreadyTaken($wallet->getId());
        }
        $this->mongoClient->put($wallet);
    }

    public function get(string $walletId): Wallet
    {
        /** @var Wallet $wallet */
        $wallet = $this->mongoClient->get($walletId);

        return $wallet;
    }

    public function findWalletsForGroupSortedByDateDescending(string $groupId, int $rows, int $offset): array
    {
        $query = ['groupId' => $groupId];

        return $this->mongoClient->findBy($query, $rows, $offset, ['creationDate' => -1]);
    }
}
