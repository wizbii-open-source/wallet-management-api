<?php

namespace App\Repository;

use App\Model\Wallet\Wallet;

interface WalletRepository
{
    public function create(Wallet $wallet): void;

    public function get(string $walletId): Wallet;

    /** @return Wallet[] */
    public function findWalletsForGroupSortedByDateDescending(string $groupId, int $rows, int $offset): array;
}
