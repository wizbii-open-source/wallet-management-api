<?php

namespace App\Repository;

use App\Exception\TransactionIdAlreadyTaken;
use App\Model\Transaction\SignedTransaction;
use Wizbii\MongoBundle\MongoClientBuilderInterface;
use Wizbii\MongoBundle\MongoClientInterface;

class MongoSignedTransactionRepository implements SignedTransactionRepository
{
    private MongoClientInterface $mongoClient;

    public function __construct(MongoClientBuilderInterface $mongoClientBuilder)
    {
        $this->mongoClient = $mongoClientBuilder->buildFor('wallet', 'transaction', SignedTransaction::class);
    }

    public function create(SignedTransaction $transaction)
    {
        if ($this->mongoClient->has($transaction->getId())) {
            throw new TransactionIdAlreadyTaken($transaction->getId());
        }
        $this->mongoClient->put($transaction);
    }

    public function get(string $transactionId): SignedTransaction
    {
        /** @var SignedTransaction $transaction */
        $transaction = $this->mongoClient->get($transactionId);

        return $transaction;
    }

    /** @return SignedTransaction[] */
    public function findTransactionsSortedByDateDescending(string $walletId, int $rows = 10, int $offset = 0): array
    {
        $query = ['$or' => [['senderWalletId' => $walletId], ['receiverWalletId' => $walletId]]];

        return $this->mongoClient->findBy($query, $rows, $offset, ['creationDate' => -1]);
    }
}
