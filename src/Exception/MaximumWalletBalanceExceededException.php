<?php

namespace App\Exception;

class MaximumWalletBalanceExceededException extends \Exception
{
    public function __construct(float $maximumBalance, float $transactionAmount)
    {
        parent::__construct("Maximum wallet balance ($maximumBalance) will be reached with a transaction of $transactionAmount", ExceptionCode::MAXIMUM_WALLET_BALANCE_EXCEEDED);
    }
}
