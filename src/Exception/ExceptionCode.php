<?php

namespace App\Exception;

interface ExceptionCode
{
    const UNSUPPORTED_TRANSACTION_TYPE = 1;
    const MAXIMUM_WALLET_BALANCE_EXCEEDED = 2;
    const MINIMUM_WALLET_BALANCE_EXCEEDED = 3;
}
