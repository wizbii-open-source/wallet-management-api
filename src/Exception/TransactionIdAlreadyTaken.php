<?php

namespace App\Exception;

use Throwable;

class TransactionIdAlreadyTaken extends \Exception
{
    public function __construct($transactionId = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct("$transactionId is already taken. Cannot override", $code, $previous);
    }
}
