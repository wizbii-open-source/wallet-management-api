<?php

namespace App\Signature;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;

/**
 * This class is intended for demo-only. Do not use in any production ready application.
 */
class StaticSignature implements SignatureAlgorithm
{
    private const SIGNATURE_NAME = 'static';
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function sign(RequestedTransaction $transaction): Signature
    {
        return new Signature(self::SIGNATURE_NAME, $this->value);
    }

    public function isValid(SignedTransaction $transaction): bool
    {
        return $transaction->getSignature()->getValue() === $this->value;
    }

    public function supports(Signature $signature): bool
    {
        return self::SIGNATURE_NAME === $signature->getAlgorithm();
    }
}
