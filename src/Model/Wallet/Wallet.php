<?php

namespace App\Model\Wallet;

use App\Exception\MaximumWalletBalanceExceededException;
use App\Exception\MinimumWalletBalanceExceededException;
use App\Model\Transaction\SignedTransaction;
use Wizbii\JsonSerializerBundle\ArraySerializable;

class Wallet implements ArraySerializable
{
    const STATUS_ACTIVE = 'active';
    const STATUS_LOCKED = 'locked';

    private string $id;
    private string $status;
    private string $groupId;
    private float $balance;
    private ?float $lowerLimit;
    private ?float $upperLimit;
    private \DateTime $creationDate;

    public function __construct(string $id, string $status, string $groupId, float $balance, ?float $lowerLimit, ?float $upperLimit, \DateTime $creationDate)
    {
        $this->id = $id;
        $this->status = $status;
        $this->groupId = $groupId;
        $this->balance = $balance;
        $this->lowerLimit = $lowerLimit;
        $this->upperLimit = $upperLimit;
        $this->creationDate = $creationDate;
    }

    public function serialize(): array
    {
        return [
            '_id' => $this->id,
            'status' => $this->status,
            'groupId' => $this->groupId,
            'balance' => $this->balance,
            'lowerLimit' => $this->lowerLimit,
            'upperLimit' => $this->upperLimit,
            'creationDate' => $this->creationDate->format(self::DATETIME_FORMAT),
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self(
            $contentAsArray['_id'],
            $contentAsArray['status'],
            $contentAsArray['groupId'],
            $contentAsArray['balance'],
            $contentAsArray['lowerLimit'],
            $contentAsArray['upperLimit'],
            new \DateTime($contentAsArray['creationDate'])
        );
    }

    /**
     * @throws MaximumWalletBalanceExceededException
     */
    public function assertIncomingTransactionIsValid(SignedTransaction $transaction)
    {
        if (isset($this->upperLimit) && $this->balance + $transaction->getAmount() > $this->upperLimit) {
            throw new MaximumWalletBalanceExceededException($this->upperLimit, $transaction->getAmount());
        }
    }

    /**
     * @throws MinimumWalletBalanceExceededException
     */
    public function assertOutgoingTransactionIsValid(SignedTransaction $transaction)
    {
        if (isset($this->lowerLimit) && $this->balance - $transaction->getAmount() < $this->lowerLimit) {
            throw new MinimumWalletBalanceExceededException($this->lowerLimit, $transaction->getAmount());
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getGroupId(): string
    {
        return $this->groupId;
    }

    public function getBalance(): float
    {
        return $this->balance;
    }

    public function getLowerLimit(): ?float
    {
        return $this->lowerLimit;
    }

    public function getUpperLimit(): ?float
    {
        return $this->upperLimit;
    }

    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
}
