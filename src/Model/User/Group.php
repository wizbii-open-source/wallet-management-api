<?php

namespace App\Model\User;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class Group implements ArraySerializable
{
    private string $id;
    private string $name;
    /** @var Permission[] */
    private array $permissions;

    public function __construct(string $id, string $name, array $permissions)
    {
        $this->id = $id;
        $this->name = $name;
        $this->permissions = $permissions;
    }

    public function serialize(): array
    {
        return [
            '_id' => $this->id,
            'name' => $this->name,
            'permissions' => array_map(fn (Permission $permission) => $permission->serialize(), $this->permissions),
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self(
            $contentAsArray['_id'],
            $contentAsArray['name'],
            array_map(fn ($conf) => Permission::deserialize($conf), $contentAsArray['permissions']),
        );
    }

    public function userCanReadWallet(string $userId): bool
    {
        return !empty(array_filter($this->permissions, fn (Permission $permission) => $permission->canReadWallet($userId)));
    }

    public function userCanCreateTransaction(string $userId): bool
    {
        return !empty(array_filter($this->permissions, fn (Permission $permission) => $permission->canCreateTransaction($userId)));
    }
}
