<?php

namespace App\Model\User;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class Permission implements ArraySerializable
{
    const TYPE_READ_WALLET = 'readWallet';
    const TYPE_CREATE_TRANSACTION = 'createTransaction';

    private string $userId;
    private array $types;

    public function __construct(string $userId, array $types)
    {
        $this->userId = $userId;
        $this->types = $types;
    }

    public function serialize(): array
    {
        return [
            'userId' => $this->userId,
            'types' => $this->types,
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self($contentAsArray['userId'], $contentAsArray['types']);
    }

    public function canReadWallet(string $userId): bool
    {
        return $this->userId === $userId && in_array(self::TYPE_READ_WALLET, $this->types);
    }

    public function canCreateTransaction(string $userId): bool
    {
        return $this->userId === $userId && in_array(self::TYPE_CREATE_TRANSACTION, $this->types);
    }
}
