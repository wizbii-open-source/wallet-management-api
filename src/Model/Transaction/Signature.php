<?php

namespace App\Model\Transaction;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class Signature implements ArraySerializable
{
    private string $algorithm;
    private string $value;

    public function __construct(string $algorithm, string $value)
    {
        $this->algorithm = $algorithm;
        $this->value = $value;
    }

    public function serialize(array $groups = []): array
    {
        return [
            'algorithm' => $this->algorithm,
            'value' => $this->value,
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self($contentAsArray['algorithm'], $contentAsArray['value']);
    }

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
