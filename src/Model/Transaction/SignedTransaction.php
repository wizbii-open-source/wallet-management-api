<?php

namespace App\Model\Transaction;

use Wizbii\JsonSerializerBundle\ArraySerializable;

class SignedTransaction extends RequestedTransaction implements ArraySerializable
{
    private string $id;
    private Signature $signature;

    private function __construct(string $id, Signature $signature, string $senderWalletId, string $receiverWalletId, \DateTime $creationDate, float $amount, ?string $comment, array $data)
    {
        parent::__construct($senderWalletId, $receiverWalletId, $creationDate, $amount, $comment, $data);
        $this->id = $id;
        $this->signature = $signature;
    }

    public static function create(string $id, Signature $signature, RequestedTransaction $requestedTransaction): SignedTransaction
    {
        return new self(
            $id,
            $signature,
            $requestedTransaction->getSenderWalletId(),
            $requestedTransaction->getReceiverWalletId(),
            $requestedTransaction->getCreationDate(),
            $requestedTransaction->getAmount(),
            $requestedTransaction->getComment(),
            $requestedTransaction->getData()
        );
    }

    public function serialize(array $groups = []): array
    {
        return [
            '_id' => $this->id,
            'signature' => $this->signature->serialize($groups),
            'senderWalletId' => $this->senderWalletId,
            'receiverWalletId' => $this->receiverWalletId,
            'creationDate' => $this->creationDate->format(self::DATETIME_FORMAT),
            'amount' => $this->amount,
            'comment' => $this->comment,
            'data' => $this->data,
        ];
    }

    public static function deserialize(array $contentAsArray)
    {
        return new self(
            $contentAsArray['_id'],
            Signature::deserialize($contentAsArray['signature']),
            $contentAsArray['senderWalletId'],
            $contentAsArray['receiverWalletId'],
            new \DateTime($contentAsArray['creationDate']),
            $contentAsArray['amount'],
            $contentAsArray['comment'],
            $contentAsArray['data']
        );
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSignature(): Signature
    {
        return $this->signature;
    }
}
