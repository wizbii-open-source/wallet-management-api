<?php

namespace App\Resources;

use App\Model\Transaction\SignedTransaction;

class Transaction implements \JsonSerializable
{
    public string $id;
    public string $senderWalletId;
    public string $receiverWalletId;
    public \DateTime $creationDate;
    public float $amount;
    public ?string $comment;
    public array $data;

    private function __construct(string $id, string $senderWalletId, string $receiverWalletId, \DateTime $creationDate, float $amount, ?string $comment, array $data)
    {
        $this->id = $id;
        $this->senderWalletId = $senderWalletId;
        $this->receiverWalletId = $receiverWalletId;
        $this->creationDate = $creationDate;
        $this->amount = $amount;
        $this->comment = $comment;
        $this->data = $data;
    }

    public static function buildOne(SignedTransaction $signedTransaction): Transaction
    {
        return new Transaction(
            $signedTransaction->getId(),
            $signedTransaction->getSenderWalletId(),
            $signedTransaction->getReceiverWalletId(),
            $signedTransaction->getCreationDate(),
            $signedTransaction->getAmount(),
            $signedTransaction->getComment(),
            $signedTransaction->getData()
        );
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'senderWalletId' => $this->senderWalletId,
            'receiverWalletId' => $this->receiverWalletId,
            'creationDate' => $this->creationDate->format(\DateTime::ISO8601),
            'amount' => $this->amount,
            'comment' => $this->comment,
            'data' => $this->data,
        ];
    }

    /**
     * @param SignedTransaction[] $signedTransactions
     *
     * @return Transaction[]
     */
    public static function buildMultiple(array $signedTransactions): array
    {
        return array_map(fn (SignedTransaction $signedTransaction) => self::buildOne($signedTransaction), $signedTransactions);
    }
}
