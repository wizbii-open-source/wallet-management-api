<?php

namespace App\Tests\Repository;

use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;
use App\Repository\MongoSignedTransactionRepository;
use Tests\Wizbii\MongoBundle\MongoClientTestCase;

class MongoTransactionRepositoryTest extends MongoClientTestCase
{
    public function test_it_can_create_and_retrieve_transaction()
    {
        $signedTransactionRepository = new MongoSignedTransactionRepository($this->getMongoClientBuilder());
        $signedTransactionRepository->create($this->getSignedTransaction('abc'));
        $signedTransaction = $signedTransactionRepository->get('abc');
        $this->assertThat($signedTransaction, $this->isInstanceOf(SignedTransaction::class));
        $this->assertThat($signedTransaction->getSignature()->getValue(), $this->equalTo('abcd'));
    }

    public function test_it_can_find_transactions_for_a_given_wallet_id()
    {
        $signedTransactionRepository = new MongoSignedTransactionRepository($this->getMongoClientBuilder());
        $signedTransactionRepository->create($this->getSignedTransaction('abc', 'remi', 'mark', '2019-12-03T09:41:33+0100'));
        $signedTransactionRepository->create($this->getSignedTransaction('def', 'mark', 'john', '2019-12-02T09:41:33+0100'));
        $signedTransactionRepository->create($this->getSignedTransaction('ghi', 'remi', 'mark', '2019-12-01T09:41:33+0100'));
        $signedTransactionRepository->create($this->getSignedTransaction('jkl', 'mark', 'remi', '2019-12-04T09:41:33+0100'));
        $signedTransactions = $signedTransactionRepository->findTransactionsSortedByDateDescending('remi');
        $this->assertThat($signedTransactions, $this->countOf(3));
        $this->assertThat($signedTransactions[0]->getId(), $this->equalTo('jkl'));
        $this->assertThat($signedTransactions[1]->getId(), $this->equalTo('abc'));
        $this->assertThat($signedTransactions[2]->getId(), $this->equalTo('ghi'));
    }

    private function getSignedTransaction(string $id, string $senderWalletId = 'remi', string $receiverWalletId = 'mark', string $date = '2019-11-30T09:41:33+0100'): SignedTransaction
    {
        return SignedTransaction::create(
            $id,
            new Signature('static', 'abcd'),
            new RequestedTransaction(
                $senderWalletId,
                $receiverWalletId,
                new \DateTime($date),
                2.0,
                null,
                []
            )
        );
    }
}
