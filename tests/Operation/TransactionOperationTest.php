<?php

namespace App\Tests\Operation;

use App\Exception\MaximumWalletBalanceExceededException;
use App\Exception\MinimumWalletBalanceExceededException;
use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Wallet\Wallet;
use App\Operation\TransactionOperation;
use App\Repository\WalletRepository;
use App\Resources\Transaction;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TransactionOperationTest extends KernelTestCase
{
    private TransactionOperation $transactionOperation;

    protected function setUp(): void
    {
        parent::setUp();
        $this->bootKernel();
        /** @var WalletRepository $walletRepository */
        $walletRepository = self::$kernel->getContainer()->get(WalletRepository::class);
        $this->transactionOperation = new TransactionOperation($walletRepository);
    }

    /** @group transaction-operation */
    public function test_it_can_create_valid_transaction()
    {
        // both wallets need to be injected somehow
        $senderWallet = new Wallet('wallet-a', 'active', 'group-a', 100.0, 0.0, 1000.0, new \DateTime('2019-11-30T09:41:33+0100'));
        $receiverWallet = new Wallet('wallet-b', 'active', 'group-b', 30.0, 0.0, 1000.0, new \DateTime('2019-11-30T10:41:33+0100'));

        $requestedTransaction = new RequestedTransaction('wallet-a', 'wallet-b', new \DateTime('2020-01-14T13:34:21+0100'), 10.0, 'my paiement', []);
        $signedTransaction = $this->transactionOperation->createTransaction($requestedTransaction);
        $this->assertThat($signedTransaction, $this->isInstanceOf(Transaction::class));
        $this->assertThat($signedTransaction->getSenderWalletId(), $this->equalTo('wallet-a'));
        $this->assertThat($signedTransaction->getReceiverWalletId(), $this->equalTo('wallet-b'));
        $this->assertThat($signedTransaction->getAmount(), $this->equalTo(10.0));
        $this->assertThat($signedTransaction->getSignature(), $this->isInstanceOf(Signature::class));
    }

    /** @group transaction-operation */
    public function test_it_rejects_transactions_when_sender_has_not_enough_funds()
    {
        $this->expectException(MinimumWalletBalanceExceededException::class);
        // both wallets need to be injected somehow
        $senderWallet = new Wallet('wallet-a', 'active', 'group-a', 100.0, 0.0, 1000.0, new \DateTime('2019-11-30T09:41:33+0100'));
        $receiverWallet = new Wallet('wallet-b', 'active', 'group-b', 30.0, 0.0, 1000.0, new \DateTime('2019-11-30T10:41:33+0100'));

        $requestedTransaction = new RequestedTransaction('wallet-a', 'wallet-b', new \DateTime('2020-01-14T13:34:21+0100'), 200.0, 'my paiement', []);
        $this->transactionOperation->createTransaction($requestedTransaction);
    }

    /** @group transaction-operation */
    public function test_it_rejects_transactions_when_receiver_will_exceed_its_upper_limit()
    {
        $this->expectException(MaximumWalletBalanceExceededException::class);
        // both wallets need to be injected somehow
        $senderWallet = new Wallet('wallet-a', 'active', 'group-a', 100.0, 0.0, 1000.0, new \DateTime('2019-11-30T09:41:33+0100'));
        $receiverWallet = new Wallet('wallet-b', 'active', 'group-b', 30.0, 0.0, 100.0, new \DateTime('2019-11-30T10:41:33+0100'));

        $requestedTransaction = new RequestedTransaction('wallet-a', 'wallet-b', new \DateTime('2020-01-14T13:34:21+0100'), 80.0, 'my paiement', []);
        $this->transactionOperation->createTransaction($requestedTransaction);
    }
}
