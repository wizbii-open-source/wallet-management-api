<?php

namespace App\Tests\Model\User;

use App\Model\User\Group;
use App\Model\User\Permission;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class GroupTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return Group::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/group';
    }

    public function test_it_can_validate_that_an_user_can_read_a_wallet()
    {
        $group = new Group('wizbii', 'Wizbii Team', [
            new Permission('remi-alvado', ['readWallet', 'createTransaction']),
        ]);
        $this->assertThat($group->userCanReadWallet('remi-alvado'), $this->isTrue());
        $this->assertThat($group->userCanReadWallet('anyone-else'), $this->isFalse());
    }

    public function test_it_can_validate_that_an_user_can_create_transaction()
    {
        $group = new Group('wizbii', 'Wizbii Team', [
            new Permission('remi-alvado', ['readWallet', 'createTransaction']),
        ]);
        $this->assertThat($group->userCanCreateTransaction('remi-alvado'), $this->isTrue());
        $this->assertThat($group->userCanCreateTransaction('anyone-else'), $this->isFalse());
    }
}
