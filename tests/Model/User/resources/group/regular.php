<?php

return new \App\Model\User\Group('wizbii', 'Wizbii Team', [
    new \App\Model\User\Permission('remi-alvado', ['readWallet', 'createTransaction']),
]);
