<?php

namespace App\Tests\Model\Transaction;

use App\Model\Transaction\Signature;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class SignatureTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return Signature::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/signature';
    }
}
