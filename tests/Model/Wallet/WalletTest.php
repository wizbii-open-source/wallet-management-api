<?php

namespace App\Tests\Model\Transaction;

use App\Exception\MaximumWalletBalanceExceededException;
use App\Exception\MinimumWalletBalanceExceededException;
use App\Model\Transaction\RequestedTransaction;
use App\Model\Transaction\Signature;
use App\Model\Transaction\SignedTransaction;
use App\Model\Wallet\Wallet;
use Tests\Wizbii\JsonSerializerBundle\ModelTestCase;

class WalletTest extends ModelTestCase
{
    protected function getSystemUnderTestClassName(): string
    {
        return Wallet::class;
    }

    protected function getTestFileBasePath(): string
    {
        return __DIR__.'/resources/wallet';
    }

    public function test_it_can_assert_an_incoming_transaction_is_valid_for_wallet_without_upper_limit()
    {
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 3.5, 0.0, null, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(10.0);
        $wallet->assertIncomingTransactionIsValid($transaction);
        $this->assertTrue(true); // avoid non successful test
    }

    public function test_it_can_assert_an_incoming_transaction_is_valid_for_wallet_with_an_upper_limit()
    {
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 3.5, 0.0, 15.0, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(10.0);
        $wallet->assertIncomingTransactionIsValid($transaction);
        $this->assertTrue(true); // avoid non successful test
    }

    public function test_it_can_assert_an_incoming_transaction_is_invalid_if_upper_limit_is_reached()
    {
        $this->expectException(MaximumWalletBalanceExceededException::class);
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 5.0, 0.0, 15.0, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(11.0);
        $wallet->assertIncomingTransactionIsValid($transaction);
    }

    public function test_it_can_assert_an_outgoing_transaction_is_valid_for_wallet_without_lower_limit()
    {
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 3.5, null, null, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(10.0);
        $wallet->assertOutgoingTransactionIsValid($transaction);
        $this->assertTrue(true); // avoid non successful test
    }

    public function test_it_can_assert_an_outgoing_transaction_is_valid_for_wallet_with_a_lower_limit()
    {
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 10.0, null, null, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(10.0);
        $wallet->assertOutgoingTransactionIsValid($transaction);
        $this->assertTrue(true); // avoid non successful test
    }

    public function test_it_can_assert_an_outgoing_transaction_is_invalid_if_lower_limit_is_reached()
    {
        $this->expectException(MinimumWalletBalanceExceededException::class);
        $wallet = new Wallet('wallet-abcd', 'active', 'wizbii', 10.0, 5.0, null, new \DateTime('2019-11-30T09:41:33+0100'));
        $transaction = $this->getSignedTransactionWithAmount(10.0);
        $wallet->assertOutgoingTransactionIsValid($transaction);
    }

    private function getSignedTransactionWithAmount(float $amount): SignedTransaction
    {
        return SignedTransaction::create(
            'transaction-abcd',
            new Signature('static', 'abcd'),
            new RequestedTransaction(
                'wallet-abcd',
                'wallet-efgh',
                new \DateTime('2019-11-30T09:41:33+0100'),
                $amount,
                null,
                []
            )
        );
    }
}
