<?php

return new App\Model\Wallet\Wallet(
    'wallet-abcd',
    'active',
    'wizbii',
    3.5,
    0.0,
    null,
    new \DateTime('2019-11-30T09:41:33+0100')
);
